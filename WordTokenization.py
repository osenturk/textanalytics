# pattern   matches         example
# \w+       word            'Magic'
# \d        digit           9
# \s        space           ''
# .*        wildcard        'username74'
# + or *    greedy match    'aaaaa'
# \S        not space       'no_spaces'         negate
# [a-z]     lowercase group 'abcdefg'

import pandas as pd

## Import the regex module
import re

my_string="Let's write RegEx!  Won't that be fun?  I sure think so.  Can you find 4 sentences?  Or perhaps, all 19 words?"

# Write a pattern to match sentence endings: sentence_endings
sentence_endings = r"[.,?,!]"

# Split my_string on sentence endings and print the result
print(re.split(sentence_endings, my_string))

# Find all capitalized words in my_string and print the result
capitalized_words = r"[A-Z]\w+"
print(re.findall(capitalized_words, my_string))

# Split my_string on spaces and print the result
spaces = r"\s+"
print(re.split(spaces, my_string))

# Find all digits in my_string and print the result
digits = r"\d+"
print(re.findall(digits, my_string))

## Tokenization with NLTK

# SEARCH: if you need to find a pattern that might not be at the beginning of the string, you should use search
# MATCH:  if you want to be specific about the composition of the entire string, at least initial patterns
import nltk
from nltk.tokenize import sent_tokenize
from nltk.tokenize import word_tokenize

nltk.download('punkt')

scene_one = open("data/grail.txt").read()

sentences = sent_tokenize(scene_one)

tokenize_sent = word_tokenize(sentences[3])

unique_tokens = set(word_tokenize(scene_one))

print(unique_tokens)

#print(scene_one)

## More regex and re.search()

# Search for the first occurrence of "coconuts" in scene_one: match
match = re.search("coconuts", scene_one)

# Print the start and end indexes of match
print(match.start(), match.end())

# Write a regular expression to search for anything in square brackets: pattern1
pattern1 = r"\[.*\]"

# Use re.search to find the first text in square brackets
print(re.search(pattern1, scene_one))

# Find the script notation at the beginning of the fourth sentence and print it
pattern2 = r"ARTHUR:"
print(re.match(pattern2, sentences[3]))

## Advanced tokenization

# OR is represented by |  -> to use the or, you can define a group using paranthesis
# ()
# [] you can define explicit character ranges using []

# pattern       matches                                         example
# [A-Za-z]      upper and lowercase English alphabet            'ABCDEFghijkl'
# [0-9]         numbers from 0 to 9                             9
# [A-Za-z\-\.]  upper and lowercase English alphabet, - and     .
# (a-z)         a, - and z    (exact matches)                   'a-z'
# (\s+|,)       spaces or a comma                               ', '

## Regex with NLTK tokenization, comprehensions

tweets=['This is the best #nlp exercise ive found online! #python',
 '#NLP is super fun! <3 #learning',
 'Thanks @datacamp :) #nlp #python']

# Import the necessary modules
from nltk.tokenize import regexp_tokenize
from nltk.tokenize import TweetTokenizer

# Define a regex pattern to find hashtags: pattern1
pattern1 = r"#\w+"

# Use the pattern on the first tweet in the tweets list
regexp_tokenize(tweets[0],pattern1)

# Write a pattern that matches both mentions and hashtags
pattern2 = r"([@|#]\w+)"

# Use the pattern on the last tweet in the tweets list
regexp_tokenize(tweets[-1], pattern2)

# Use the TweetTokenizer to tokenize all tweets into one list
tknzr = TweetTokenizer()
all_tokens = [tknzr.tokenize(t) for t in tweets]
print(all_tokens)

## Non-ascii tokenization
german_text="Wann gehen wir Pizza essen? 🍕 Und fährst du mit Über? 🚕"

# Tokenize and print all words in german_text
all_words = word_tokenize(german_text)
print(all_words)

# Tokenize and print only capital words
capital_words = r"[A-Z|Ü]\w+"
print(regexp_tokenize(german_text,capital_words))

# Tokenize and print only emoji
emoji = "['\U0001F300-\U0001F5FF'|'\U0001F600-\U0001F64F'|'\U0001F680-\U0001F6FF'|'\u2600-\u26FF\u2700-\u27BF']"
print(regexp_tokenize(german_text, emoji))

## Charting practice
# note: question mark makes the preceding character optional

import matplotlib.pyplot as plt

lines = scene_one.split('\n')


# Replace all script lines for speaker
pattern = "[A-Z]{2,}(\s)?(#\d)?([A-Z]{2,})?:"
lines = [re.sub(pattern, '', l) for l in lines]
print(lines)
# Tokenize each line: tokenized_lines
tokenized_lines = [regexp_tokenize(s,"\w+") for s in lines]

# Make a frequency list of lengths: line_num_words
line_num_words = [len(t_line) for t_line in tokenized_lines]

# Plot a histogram of the line lengths
# plt.hist(line_num_words)

# plt.show()

## bag-of-words, tokenization

import nltk
from collections import Counter

nltk.download('punkt')

article = open("data/wikipedia_articles/wiki_text_debugging.txt").read()

tokens = word_tokenize(article)

lower_tokens = [t.lower() for t in tokens]

bow_simple = Counter(lower_tokens)

print(bow_simple.most_common(10))

## Text preprocessing practice
from nltk.stem import WordNetLemmatizer
nltk.download('wordnet')

english_stops = open("data/english_stopwords.txt").read()
alpha_only = [t for t in lower_tokens if t.isalpha()]

no_stops = [t for t in alpha_only if t not in english_stops]

wordnet_lemmatizer = WordNetLemmatizer()

lemmatized = [wordnet_lemmatizer.lemmatize(t) for t in no_stops]

bow = Counter(lemmatized)

print(bow.most_common(10))

## gensim, nltk, word vectors, topic identification
## gensim allows you build a corpra
## pipeline in preprocessing

import glob
list_of_files = glob.glob('data/wikipedia_articles/*.txt')

# encoding = "ISO-8859-1"
articles=[]
for file in list_of_files:
 text_article = open(file,'r', encoding="utf-8").read()
 tokens = word_tokenize(text_article)
 lower_tokens = [t.lower() for t in tokens]
 alpha_only = [t for t in lower_tokens if t.isalpha()]
 no_stops = [t for t in alpha_only if t not in english_stops]
 # wordnet_lemmatizer = WordNetLemmatizer()
 # lemmatized = [wordnet_lemmatizer.lemmatize(t) for t in no_stops]
 # bow = Counter(lemmatized)

 articles.append(no_stops)
 # print(lambda one: text_article+":"+one, no_stops)
print("preprocessing: ")
print(articles[1])


## Import Dictionary, Gensim
##
from gensim.corpora.dictionary import Dictionary

# Create a Dictionary from the articles: dictionary
dictionary = Dictionary(articles)

# Select the id for "computer": computer_id
computer_id = dictionary.token2id.get("computer")

# Use computer_id with the dictionary to print the word
print(dictionary.get(computer_id))

# Create a MmCorpus: corpus
corpus = [dictionary.doc2bow(article) for article in articles]


# Print the first 10 word ids with their frequency counts from the fifth document
print(corpus[1][:10])

##Gensim, bag of words

from collections import defaultdict
import itertools as it

# Save the fifth document: doc
doc = corpus[1]

print("printing doc:")
print(doc)
# Sort the doc for frequency: bow_doc
bow_doc = sorted(doc, key=lambda w: w[1], reverse=True)

print("printing bowdoc")
print(bow_doc)

# Print the top 5 words of the document alongside the count
for word_id, word_count in bow_doc[:5]:
 print(dictionary.get(word_id), word_count)

# Create the defaultdict: total_word_count
total_word_count = defaultdict(int)
for word_id, word_count in it.chain.from_iterable(corpus):
 total_word_count[word_id] += word_count

# Create a sorted list from the defaultdict: sorted_word_count
sorted_word_count = sorted(total_word_count.items(), key=lambda w: w[1], reverse=True)

# Print the top 5 words across all documents alongside the count
for word_id, word_count in sorted_word_count[:5]:
 print(dictionary.get(word_id), word_count)

# Gensim, td-idf, inverse document frequency, allows you to determine
# the most important words in each document

## Tf-idf with Wikipedia
# Import TfidfModel
from gensim.models.tfidfmodel import TfidfModel

# Create a new TfidfModel using the corpus: tfidf
tfidf = TfidfModel(corpus)

# Calculate the tfidf weights of doc: tfidf_weights
tfidf_weights = tfidf[doc]

# Print the first five weights
print("print top 5 weighted")
print(tfidf_weights[:5])

# Sort the weights from highest to lowest: sorted_tfidf_weights
sorted_tfidf_weights = sorted(tfidf_weights, key=lambda w: w[1], reverse=True)

print("gensim tf-idf - top 5 weights")
# Print the top 5 weighted words
for term_id, weight in sorted_tfidf_weights[:5]:
    print(dictionary[term_id], weight)

## Name Entity recognition
## NER with NLTK
import nltk
nltk.download('averaged_perceptron_tagger')
nltk.download('maxent_ne_chunker')
nltk.download('words')

news_article = open("data/news_articles/uber_apple.txt",'r', encoding="utf-8").read()

# sentences = nltk.sent_tokenize(news_article)
# Tokenize the article into sentences: sentences
sentences = nltk.sent_tokenize(news_article)

# Tokenize each sentence into words: token_sentences
token_sentences = [nltk.word_tokenize(sent) for sent in sentences]

# Tag each tokenized sentence into parts of speech: pos_sentences
pos_sentences = [nltk.pos_tag(sent) for sent in token_sentences]

# Create the named entity chunks: chunked_sentences
chunked_sentences = nltk.ne_chunk_sents(pos_sentences, binary=True)

# Test for stems of the tree with 'NE' tags
for sent in chunked_sentences:
    for chunk in sent:
        if hasattr(chunk, "label") and chunk.label() == "NE":
            print(chunk)

##Charting practice

# Create the named entity chunks: chunked_sentences
chunked_sentences = nltk.ne_chunk_sents(pos_sentences, binary=False)
#chunked_sentences = open("data/chunked_sentences.txt").read()

# Create the defaultdict: ner_categories
ner_categories = defaultdict(int)

print("chunked sentences")
#print(chunked_sentences)
# Create the nested for loop
for sent in chunked_sentences:
    for chunk in sent:
        if hasattr(chunk, "label"):
            print(chunk)
            ner_categories[chunk.label()] += 1


# Create a list from the dictionary keys for the chart labels: labels
labels = list(ner_categories.keys())
print("printing labels...")

# Create a list of the values: values
values = [ner_categories.get(l) for l in labels]

print("printing values...")
print(values)
# Create the pie chart
plt.pie(values, labels=labels, autopct='%1.1f%%', startangle=140)

# Display the chartw
plt.show()

## Spacy, NLP, Similar to Gensim, Displacy
##  python -m spacy download en

# Import spacy
import spacy
# Instantiate the English model: nlp
nlp = spacy.load('en',tagger=False,parser=False,matcher=False)

article = open("data/news_articles/uber_apple.txt",'r', encoding="utf-8").read()

# Create a new document: doc
doc = nlp(article)


print("printing entities with spacy...")

# Print all of the found entities and their labels
for ent in doc.ents:
    print(ent.label_, ent.text)

# polyglot
from polyglot.text import Text

article = open("data/news_articles/french.txt",'r', encoding="utf-8").read()
# Create a new text object using Polyglot's Text class: txt
txt = Text(article)

# Print each of the entities found
for ent in txt.entities:
    print(ent)

print(type(ent))


## Classification



### CountVectorizer for text classification

# In this exercise, you'll use pandas alongside scikit-learn
# to create a sparse text vectorizer you can use to train and
# test a simple supervised model. To begin, you'll set up a CountVectorizer
# and investigate some of its features.

#Import the necessary modules
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split

df=pd.read_csv('data/fake_or_real_news.csv')

# Print the head of df
print(df.head())

# Create a series to store the labels: y
y = df.label

# Create training and test sets
X_train, X_test, y_train, y_test = train_test_split(df['text'],y,test_size=0.33,random_state=53)

# Initialize a CountVectorizer object: count_vectorizer
count_vectorizer = CountVectorizer(stop_words="english")

# Transform the training data using only the 'text' column values: count_train
count_train = count_vectorizer.fit_transform(X_train,y_train)

# Transform the test data using only the 'text' column values: count_test
count_test = count_vectorizer.transform(X_test)

# Print the first 10 features of the count_vectorizer
print(count_vectorizer.get_feature_names()[:10])


### TfidfVectorizer for text classification

# Similar to the sparse CountVectorizer created in the previous exercise,
# you'll work on creating tf-idf vectors for your documents.
# You'll set up a TfidfVectorizer and investigate some of its features.

# In this exercise, you'll use pandas and sklearn along with
# the same X_train, y_train and X_test, y_test DataFrames
# and Series you created in the last exercise.


# Import TfidfVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer

# Initialize a TfidfVectorizer object: tfidf_vectorizer
tfidf_vectorizer = TfidfVectorizer(stop_words="english",max_df=0.7)

# Transform the training data: tfidf_train
tfidf_train = tfidf_vectorizer.fit_transform(X_train,y_train)

# Transform the test data: tfidf_test
tfidf_test = tfidf_vectorizer.transform(X_test)

# Print the first 10 features
print(tfidf_vectorizer.get_feature_names()[:10])

# Print the first 5 vectors of the tfidf training data
print(tfidf_train.A[:5])

## Inspecting the vectors

# Create the CountVectorizer DataFrame: count_df
count_df = pd.DataFrame(count_train.A, columns=count_vectorizer.get_feature_names())

# Create the TfidfVectorizer DataFrame: tfidf_df
tfidf_df = pd.DataFrame(tfidf_train.A, columns=tfidf_vectorizer.get_feature_names())

# Print the head of count_df
print("Count Vectorizer")
print(count_df.head())

# Print the head of tfidf_df
print("TfIdf Vectorizer")
print(tfidf_df.head())

# Calculate the difference in columns: difference
difference = set(count_df) - set(tfidf_df)
print(difference)

# Check whether the DataFrames are equal
print(count_df.equals(tfidf_df))

### Naive Bayes

# commonly used for testing NLP classifications problems
# Basis in probability

# Given a particular piece of data, how likely is a particular outcome?

# Examples:
# If the plot has a spaceship, how likely is it to be sci-fi?
# if the plot has a spaceship and an alien, how likely now is it sci-fi?

# Each word from CountVectorizer acts as a feature
# Naive Bayes: Simple and effective
# MultinomialNB multiple label classification, works well with CountVectorizer

##
# Now it's your turn to train the "fake news" model using the
# features you identified and extracted. In this first exercise
# you'll train and test a Naive Bayes model using the CountVectorizer data.

# Import the necessary modules
from sklearn.naive_bayes import MultinomialNB
from sklearn import metrics

# Instantiate a Multinomial Naive Bayes classifier: nb_classifier
nb_classifier = MultinomialNB()

# Fit the classifier to the training data
nb_classifier.fit(count_train,y_train)

# Create the predicted tags: pred
pred = nb_classifier.predict(count_test)

# Calculate the accuracy score: score
score = metrics.accuracy_score(y_test,pred)
print(score)

# Calculate the confusion matrix: cm
cm = metrics.confusion_matrix(y_test,pred,labels=['FAKE','REAL'])
print(cm)

## Training and testing the "fake news" model with TfidfVectorizer
# Now that you have evaluated the model using the CountVectorizer,
# you'll do the same using the TfidfVectorizer with a Naive Bayes model.

# Create a Multinomial Naive Bayes classifier: nb_classifier
nb_classifier = MultinomialNB()

# Fit the classifier to the training data
nb_classifier.fit(tfidf_train,y_train)

# Create the predicted tags: pred
pred = nb_classifier.predict(tfidf_test)

# Calculate the accuracy score: score
score = metrics.accuracy_score(y_test,pred)
print(score)

# Calculate the confusion matrix: cm
cm = metrics.confusion_matrix(y_test,pred,labels=['FAKE','REAL'])
print(cm)

# Improving your model
# Your job in this exercise is to test a few different
# alpha levels using the Tfidf vectors to determine
# if there is a better performing combination.

import numpy as np

# Create the list of alphas: alphas

alphas = np.arange(0,1,0.1)

# Define train_and_predict()
def train_and_predict(alpha):
    # Instantiate the classifier: nb_classifier
    nb_classifier = MultinomialNB(alpha=alpha)
    # Fit to the training data
    nb_classifier.fit(tfidf_train,y_train)
    # Predict the labels: pred
    pred = nb_classifier.predict(tfidf_test)
    # Compute accuracy: score
    score = metrics.accuracy_score(y_test,pred)
    return score

# Iterate over the alphas and print the corresponding score
for alpha in alphas:
    print('Alpha: ', alpha)
    print('Score: ',train_and_predict(alpha) )
    print()

## Inspecting your model

# Now that you have built a "fake news" classifier,
# you'll investigate what it has learned. You can map the important
# vector weights back to actual words using some simple inspection techniques.

# Get the class labels: class_labels
class_labels = nb_classifier.classes_

# Extract the features: feature_names
feature_names = tfidf_vectorizer.get_feature_names()

# Zip the feature names together with the coefficient array and sort by weights: feat_with_weights
feat_with_weights = sorted(zip(nb_classifier.coef_[0], feature_names))

# Print the first class label and the top 20 feat_with_weights entries
print(class_labels[0], feat_with_weights[:20])

# Print the second class label and the bottom 20 feat_with_weights entries
print(class_labels[1], feat_with_weights[-20:])
